<?php
/**
 * 文件加载 
 */
namespace Core;

class Loader{

	/**
	 * loader 类型
	 * @var [type]
	 */
	protected $loadType = array(

        'func'   => ['fn'=> 'Func',    'ext'=> '.func.php',  'type'=>'function'], 
        // 'classs' => ['fn'=> 'class',  'ext'=> '.class.php', 'type'=>'object'], 
        'classs' => ['fn'=> 'Core',  'ext'=> '.php',       'type'=>'object'], 
        'model'  => ['fn'=> 'Library', 'ext'=> '.mod.php',   'type'=>'function'],
        'library'=> ['fn'=> 'Models',  'ext'=> '.php',       'type'=>'object']
    );

    /**
     * [$classMap description]
     * @var [type]
     */
    protected static $classMap=[];

    public $paths=[];

	/**
	 * [__construct description]
	 * @Author   Fjie
	 * @DateTime 2020-06-03
	 */
	public function __construct($loadType=null)
	{
        defined('COREPATH') || define('COREPATH', dirname(__DIR__).'/framework/');

        if(!empty($loadType) && is_array($loadType))
        {
            $this->loadType = array_merge($this->loadType, $loadType);
        }
	}

	/**
	 * 注册
	 * @Author   Fjie
	 * @DateTime 2020-06-03
	 * @param    boolean    $prepend [description]
	 */
    public function register($prepend = false)
    {
        spl_autoload_register(array($this, 'loadClass'), true, $prepend);
    }
    /**
     * 注销
     */
    public function unregister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    /**
     * [loadClass description]
     * @Author   Fjie
     * @DateTime 2020-06-03
     * @return   [type]     [description]
     */
    public function loadClass($class)
    {    
        $newObject = new $this->classMap[$class]();
        return $newObject;
    }

    /**
     * [__call description]
     * @Author   Fjie
     * @DateTime 2020-06-03
     * @param    string     $name [description]
     * @param    array      $args [description]
     */
    public function __call(string $name, array $args)
    {	
    	if( !array_key_exists($name, $this->loadType) || empty($args[0]) || !is_string($args[0]))return false;



        switch($this->loadType[$name]['type'])
        {
            case 'function': break;

            case 'object': 

                $this->register(true);  

                $this->classMap[$args[0]] = "{$this->loadType[$name]['fn']}\\{$args[0]}";
                break;
            default: 
                return false;
        }

        ;
        $this->findFile(

            dirname(dirname(__DIR__)). DIRECTORY_SEPARATOR .$this->loadType[$name]['fn'] .DIRECTORY_SEPARATOR. $args[0].$this->loadType[$name]['ext']

        );

        return $this->paths;
    
    }

    /**
     * 查找文件路径
     * @Author   Fjie
     * @DateTime 2020-06-02
     * @return   [type]     [description]
     */
    public function findFile($path)
    {   
        //临时记录 可删 
        array_push($this->paths, $path);
        // echo $path; die;
        if( !file_exists($path) )echo 'not found file';
        includeFile($path);
             
    }
}

function includeFile($file)
{
    @include_once $file;
}

function includeClassFile($file)
{
    @include_once $file;

    #use namespace
}