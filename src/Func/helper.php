<?php
use Core\Loader;

function loader()
{
	static $loader;
	if (empty($loader)) {
		$loader = new Loader();
	}
	return $loader;	
}